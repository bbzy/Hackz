# README

A minimal dark greenish for VSCode.
Good for eyeballs and looks more awesome.

## Dark Mode (MacOSX Dark: Default)

![Hackz Theme](https://preview.ibb.co/cHRM9K/Screenshot_2018_10_04_at_1_34_40_PM.png 'Theme screenshot')

## Pure Dark Mode

![Hackz Theme](https://preview.ibb.co/fE8CGz/Screenshot_2018_10_04_at_1_35_05_PM.png 'Theme screenshot')

## Change Logs
Version 1.0.5

- Changed the color of tag token of html source code.

Version 1.0.4

- Changed syntax color style.
- Changed the editor foreground color.

Version 1.0.3

- Added macOSX dark color theme.
- Adjusted some colors.

Version 1.0.2

- Added a extension icon.
